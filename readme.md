# Foundation 6 PHP Template #

Based on the Foundation 6 Zurb template with an integrated PHP server

## Prerequesitions ##

To use this template you'll need:

* [node.js](https://nodejs.org)
* [bower](http://bower.io/)
* Foundation CLI
* A PHP Installation ([XAMPP](https://www.apachefriends.org))

To install the Foundation CLI do:

```
#!sh

npm install -g foundation-cli
```
## Installation ##

### Automatic ###

You can use this [shell script](https://bitbucket.org/synmatter/foundation-php/downloads/foundation-php_setup.sh) to clone and set up a new project

### Manual ###

Clone the GIT repository or download it as ZIP

```
#!sh

git clone https://bitbucket.org/synmatter/foundation-php $project_name
```
Navigate into the directory

```
#!sh

cd $project_name
```
Install node and bower in the project

```
#!sh

npm install
bower install
```

Before you are able to use it you need to set the path to your PHP executable in the config.yml file
```
#!yml

# Your project's server will run on localhost:xxxx at this port
PORT: 8000

# Your local PHP.exe uncomment the path that fits for you or use your own

# Windows standart XAMPP install
# PHPEXEC: 'c:/xampp/php/php.exe'

# Mac standart XAMPP install
# PHPEXEC: "/Applications/XAMPP/bin/php"
```

Now your ready to go! Use it like any other Foundation template with the Foundation CLI the PHP server is integrated into the normal workflow

```
#!sh

foundation watch
```